const express = require('express');
const router = express.Router();

router.use(express.json());

const miNomina = require("./miNomina");


router.use('/miNomina', miNomina);


router.all('/', function (request, response){
    response.json({
        "Clientes":`http://localhost${process.env.PORT}/api/miNomina`
    })
})

module.exports = router;