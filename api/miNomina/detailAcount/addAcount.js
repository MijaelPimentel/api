const Cuenta = require('../../models/detailAcount');

function updatedetailAcount(req , res){
    
    const acount = req.params.acount;
    let concept =  req.body.detail[0].concept;
    let date = req.body.detail[0].date;
    let hour = req.body.detail[0].hour;
    let amount = req.body.detail[0].amount;

    Cuenta.findOne({acount:acount}, function(err, cuenta){
        if(err) return next(err);
        if(!cuenta) return res.send();
        cuenta.detail.push(
            {    
                concept:concept,
                date:date,
                hour:hour,
                amount:amount
            });
        cuenta.save()
        .then(function(result){
            res.json({
              message: 'Movimiento Registrado'
            })
          })
          .catch(function(err){
            res.json({
              error: err
            })
          })
    })
}

module.exports = updatedetailAcount