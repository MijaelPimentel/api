
const detailAcount = require('../../models/detailAcount');

function createdetailAcount(req , res){
  
  const acount = req.body.acount;
  let concept = req.body.detail[0].concept;
  let date = req.body.detail[0].date;
  let hour = req.body.detail[0].hour;
  let amount = req.body.detail[0].amount;

  let newdetailAcount = new detailAcount({
      acount:acount,
      detail:{
        concept:concept,
        date:date,
        hour:hour,
        amount:amount
      }
    });
  
    newdetailAcount.save()
      .then(function(result){
        res.json({
          message: 'Movimiento Registrado'
        })
      })
      .catch(function(err){
        res.json({
          error: err
        })
      })
}

module.exports = createdetailAcount

// acount:String,
//     detail:[{
//         concept:String,
//         date:String,
//         hour: String,
//         amount:String
//     }]