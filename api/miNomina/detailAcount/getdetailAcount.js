
const detailAcount = require('../../models/detailAcount');

function getdetailAcount(req , res){
    
    const acount = req.params.acount;

    detailAcount.find()
        .where('acount').equals(acount)
        .then(function(doc){
          res.json({
            detailAcount:doc
          })
        })
        .catch(function(err){
            res.json({
                error: err
            })
        })
      }

module.exports = getdetailAcount