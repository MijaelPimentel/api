const User = require('../../models/usuarios');
const bcypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_APP = "Userowo";

function login(req , res){
const {email, password} = req.body;
User.findOne({email})
.then(user => {
    return bcypt.compare(password,user.password)
        .then(success => ({success,user}))
})
.then(({success,user}) => {
    if(!success){
        res.status(400).json({
            message:'correo y contraseña erroneas'
        })
    }
    jwt.sign(
        {_id:user.id},
        JWT_APP,
        {expiresIn: 60*60*24},
        (err, token) => {
            if(err){
                res.json({err:err})
                throw {message:"Error, intentalo denuevo"}
            }
            res.json({token,user})
        }
        )
})
.catch(err => err);

}

module.exports = login