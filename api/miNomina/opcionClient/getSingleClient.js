//Con al siguiente Funcion se puede realizar una busqueda puntual respecto al id que se requiera buscar
const Cliente = require('../../models/Client');

function getSingleClient(req , res){
  const acount = req.params.acount;

    Cliente.find()
        .where('acount').equals(acount)
        .then(function(doc){
          res.json({
              clientes:doc
          })
        })
        .catch(function(err){
            res.json({
                error: err
            })
        })
}

module.exports = getSingleClient