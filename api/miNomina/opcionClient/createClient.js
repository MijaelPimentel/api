const Cliente = require('../../models/Client');


function createClient(req, res) {

  const acount = req.body.acount;
  const numberClient = req.body.numberClient;
  const name = req.body.name;
  const firtname = req.body.firtname;
  const secondname = req.body.secondname;
  const nationality = req.body.nationality;
  const latitude = req.body.centerJob.latitude;
  const length = req.body.centerJob.length;
  
  const newClient = new Cliente({
    acount:acount,
    numberClient:numberClient,
    name:name,
    firtname: firtname,
    secondname:secondname,
    nationality:nationality,
    centerJob:{
        latitude:latitude,
        length: length
    }
  });

  newClient.save()
    .then(function(result){
      res.json({
        message: 'Cliente Creado'
      })
    })
    .catch(function(err){
      res.json({
        error: err
      })
    })
}

module.exports = createClient;