const Cliente = require('../../models/Client');

function updateClient(req, res) {

  const id = req.params.id;
  const acount = req.body.acount;
  const numberClient = req.body.numberClient;
  const name = req.body.name;
  const firtname = req.body.firtname;
  const secondname = req.body.secondname;
  const nationality = req.body.nationality;
  const latitude = req.body.centerJob.latitude;
  const length = req.body.centerJob.length;

 Cliente.find()
  .where('_id').equals(id)
  .update({ 
    acount:acount,
    numberClient:numberClient,
    name:name,
    firtname: firtname,
    secondname:secondname,
    nationality:nationality,
    centerJob:{
        latitude:latitude,
        length: length
    }
  })
  .then(function(result){
      res.json({
        message: 'Cliente Actualizado'
      })
    })        
    .catch(function(err){
      res.json({
        error: err
      })
    })
}

module.exports = updateClient;