const Cliente = require('../../models/Client');

function deleteClient(req , res){

    const id = req.params.id;

    Cliente.find()
    .where('_id').equals(id)
    .deleteOne({ '_id': id  })
    .then(function(result){
        res.json({
          message: 'Cliente Eliminado'
        })
      })
      .catch(function(err){
        res.json({
          error: err
        })
      })
}

module.exports = deleteClient