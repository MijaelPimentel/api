const express = require('express');
const router = express.Router();

const getClient = require('./opcionClient/getClient');
const getSingleClient = require('./opcionClient/getSingleClient');
const delteClient = require('./opcionClient/delteClient');
const createClient = require('./opcionClient/createClient');
const updateClient = require('./opcionClient/updateClient');
//######
const getdetailAcount = require('./detailAcount/getdetailAcount');
const createdetailAcount = require('./detailAcount/createdetailAcount');
const updatedetailAcount = require('./detailAcount/updatedetailAcount');
//######
const singup = require('./users/singUp');
const login = require('./users/login');
const getUserlogin = require('./users/getUserlogin');
//#####
const addAcount = require('./detailAcount/addAcount');

//metodos get
router.get('/V01/getClient',getClient);
router.get('/V01/getClient/:acount',getSingleClient);
router.get('/V01/getUserLogin',getUserlogin);
//######
router.get('/V01/getdetailAcount/:acount',getdetailAcount);

//metodo delete
router.delete('/V01/delteClient/:id',delteClient);

//metodo post
router.post('/V01/createClient',createClient);
//######
router.post('/V01/createdetailAcount',createdetailAcount);
//######
router.post('/V01/singup',singup);
router.post('/V01/login',login);

//mentodo put
router.put('/V01/updateCliente/:id',updateClient);
//######
//router.put('/V01/updatedetailAcount/:id',updatedetailAcount); Funcionalidad no requerida
//agregar movimientos a una cuenta
router.put('/V01/addAcount/:acount',addAcount);


module.exports = router;