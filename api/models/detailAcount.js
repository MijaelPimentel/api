const mongoose = require('mongoose');

const detailAcountSchema = mongoose.Schema({
    acount:String,
    detail:[{
        concept: String,
        date: String,
        hour: String,
        amount: String
    }]
});

module.exports = mongoose.model('DetalleCuenta', detailAcountSchema);