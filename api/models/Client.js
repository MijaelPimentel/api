const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    acount:String,
    numberClient:Number,
    name:String,
    firtname: String,
    secondname:String,
    nationality:String,
    centerJob:{
        latitude:String,
        length: String
    }
});

module.exports = mongoose.model('Cliente', clientSchema);

// {
//     "_id": {
//         "$oid": "5d82975ea3d1820c2c93d7e2"
//     },
//     "idCliente": 1299,
//     "nombre": "Paco",
//     "apellido": "starse",
//     "listado": [
//         {
//             "fecha": "17/09/2019",
//             "importe": 33.95,
//             "categoria": "Compra comercio"
//         },
//         {
//             "fecha": "18/09/2019",
//             "importe": 44.95,
//             "categoria": "ATM"
//         }
//     ]
// }