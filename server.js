const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config(); 
const mongose = require('mongoose');
const cors = require("cors");

app.use('/',express.static('../../practitioner-proyect'));
app.use(cors());

const api = require("./api"); //traigo todo de la carpeta api

app.use('/api',api); //app.use es un middleware, cualquier peticion la delego a index.js en este caso.

app.listen(process.env.PORT, function(){
    console.log(`inicia el servidor M:${process.env.PORT}`);
    console.log(`http://localhost:${process.env.PORT}`); 
});

//conectBD, metodo mejorado para conectar con Mongo
mongose.connect(process.env.MONGO_DB, { useNewUrlParser: true })
.then(function(result){
    console.log('se conecto mongo');
})
.catch(function(err){
    console.log(err);
});