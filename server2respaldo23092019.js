//version inicial

let express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;
let cors = require('cors');
let path = require('path');
let requestjson = require('request-json');
const urlClientesMlab ="https://api.mlab.com/api/1/databases/mpimentel/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&s";
let clientesMLab = requestjson.createClient(urlClientesMlab);

let movimientosJSON = require('./movimientosv2.json')

let bodyparse = require('body-parser');
app.use(cors());
app.use(bodyparse.json());
app.use(function(req,res,next){
  /*var err = new Error('Not Found');
   err.status = 404;
   next(err);*/

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

//  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});


app.listen(port);
console.log(`http://localhost:${port}`);
console.log('todo list RESTful Backend  server started on: ' + port);



/*############################  
//#modulos con el Metodo GET #
//############################*/
app.get(`/`,getIndex);
app.get(`/Clientes/:idcliente`,getSingleClient);
app.get(`/Movimientos`,getMovimientos);
app.get(`/V02/Movimientos`, getMovimientosV2);
app.get(`/V02/Movimientos/:idmove`,getMovimientosV2SingleMove);
app.get(`/V02/Movimientosq`,getMovimientosV2Query);
app.get("/V01/Clientes",getclientesAPI)

/*############################
//#modulos con el Metodo POST #
//############################*/
app.post('/',createPost);
app.post('/v2/MovimientosCreateUser',moveCreateUser);
app.post('/V01/Clientes',createClient);

/*############################
//#modulos con el Metodo PUT #
//############################*/
app.put('/V01/Clientes/:id',editUser);

/*############################
//#modulos con el Metodo DELETE #
//############################*/
app.delete('/',deleteUser);
//############################

function getIndex(req, res){
  res.sendFile(path.join(__dirname,`index.html`));
}

function getSingleClient(req, res){
  res.send(`Aqui tiene al cliente ${req.params.idcliente}`);
}

function getMovimientos(req, res){
  res.sendfile(`movimientosv1.json`);
}

function getMovimientosV2(req, res){
  res.send(movimientosJSON);
}

function getMovimientosV2SingleMove(req, res){
  console.log(`${req.params.idmove}`)
  res.send(movimientosJSON[req.params.idmove - 1]);
}

function getMovimientosV2Query(req, res){
  console.log(`${req.query.q}`); //ejemplo de URL http://localhost:3000/V02/Movimientosq?q=hola+mundo+node
  res.send(`Query recibidos`);
}

//funciones con Metodo POST 
function createPost(req,res){
  res.send(`Hola este es el post Cambiada`);
}

function moveCreateUser(req, res){
  let newRegister = req.body;

  newRegister.id = movimientosJSON.length + 1;
  movimientosJSON.push(newRegister);
  res.send("movimiento dado de alta");

}

//funciones con metodo DELETE

function deleteUser(req,res){
  res.send(`Hola este es el delete`);
}


function getclientesAPI(req, res){

clientesMLab.get('',function(err,resM,body){
  if(err){
    console.log(body);
  }else{
    res.send(body);
  }
})
}

function createClient(req, res){
  clientesMLab.post('',req.body,function(err, resM, body){
    res.send(body);
  })
}

//funciones con metodo PUT
function editUser(req,res){
  clientesMLab.put('',req.body,function(err, resM, body){
    req.collection.update({ _id: req.params.id
    }, {$set:req.body}, {safe:true, multi:false}, function(e, result){
    if (e) return next(e);
    res.send((result === 1) ? {msg:'success'} : {msg:'error'});
    }); 
  });
}